package sobr_studio.beritabali

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import sobr_studio.beritabali.getData.NewsWebPresenter
import sobr_studio.beritabali.model.LinkBerita


class TestURL : AppCompatActivity(), TestURLView {

    lateinit var getBtn: Button
    lateinit var result: TextView
    lateinit var presenter: NewsWebPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_url)
        result = findViewById(R.id.result)
        getBtn = findViewById(R.id.getBtn)
        presenter = NewsWebPresenter(this)
        getBtn.setOnClickListener { presenter.getTribunBali() }
    }

    override fun getWebsite(data:List<LinkBerita>) {
        result.text = data.first().judul
    }
}
