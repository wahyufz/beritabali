package sobr_studio.beritabali.model

data class LinkBerita(
    var source: String? = null,
    var img: String? = null,
    var timestamp: String? = null,
    var link: String? = null,
    var judul: String?=null
)