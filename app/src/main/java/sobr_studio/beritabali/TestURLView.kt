package sobr_studio.beritabali

import sobr_studio.beritabali.model.LinkBerita

interface TestURLView{

    fun getWebsite(data:List<LinkBerita>)
}