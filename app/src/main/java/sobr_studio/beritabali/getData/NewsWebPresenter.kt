package sobr_studio.beritabali.getData

import android.util.Log
import android.widget.TextView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jsoup.Jsoup
import sobr_studio.beritabali.MainActivity
import sobr_studio.beritabali.R.id.result
import sobr_studio.beritabali.TestURL
import sobr_studio.beritabali.TestURLView
import sobr_studio.beritabali.model.LinkBerita
import java.io.IOException

class NewsWebPresenter(private val view:TestURLView){
    val data:MutableList<LinkBerita> = mutableListOf()

    fun getTribunBali() {
        doAsync {
            val builder = StringBuilder()

            try {
                val doc = Jsoup.connect("http://bali.tribunnews.com").get()
                val title = doc.title()
                val li = doc.select("#latestul li.p1520.art-list.pos_rel")
                val links = li.select("a[href].f20.ln24.fbo.txt-oev-2")
                val images = li.select("img.shou2.bgwhite")
                val timestamps = li.select("time")

                builder.append(title).append("\n")

                for (link in links) {
                    builder.append("\n").append("Link : ").append(link.attr("href"))
                        .append("\n").append("Text : ").append(link.text()).append("\n\n")
                }
                for(i in 0..39){
                    data.add(LinkBerita(source = "Tribun Bali", img = images[i].attr("src"),timestamp = timestamps[i].attr("title"),link = links[i].attr("href"),judul = links[i].text()))
                }
            } catch (e: IOException) {
                builder.append("Error : ").append(e.message).append("\n")
            }
            Log.d("links",data.toString())
            uiThread { view.getWebsite(data) }
        }

    }

}

