package sobr_studio.beritabali.tab

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import org.jetbrains.anko.find
import sobr_studio.beritabali.R
import sobr_studio.beritabali.model.LinkBerita

class ListBeritaAdapter(private val items: List<LinkBerita>, private val listener: (LinkBerita) -> Unit)
    : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_berita, parent, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position],listener)
    }

    override fun getItemCount(): Int = items.size

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val recyclerView = view.find<RelativeLayout>(R.id.rl_list_berita)
    private val source = view.find<TextView>(R.id.tv_source)
    private val img = view.find<ImageView>(R.id.iv_berita)
    private val timestamp = view.find<TextView>(R.id.tv_timestamp)
    private val judul = view.find<TextView>(R.id.tv_judul)
    private val imgRequestOptions = RequestOptions()
        .error(R.drawable.notfound)
        .placeholder(R.drawable.loading)
        .centerCrop()

    fun bindItem(item: LinkBerita, listener: (LinkBerita) -> Unit) {
        Log.d("item",item.toString()+"\n\n")


        source.text = item.source
        timestamp.text = item.timestamp
        judul.text = item.judul
        Glide.with(recyclerView)
            .load(item.img)
            .apply(imgRequestOptions)
            .into(img)

        recyclerView.setOnClickListener { listener(item) }
    }
}

