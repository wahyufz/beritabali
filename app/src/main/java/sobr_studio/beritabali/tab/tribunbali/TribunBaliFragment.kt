package sobr_studio.beritabali.tab.tribunbali


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import sobr_studio.beritabali.R
import sobr_studio.beritabali.TestURL
import sobr_studio.beritabali.TestURLView
import sobr_studio.beritabali.getData.NewsWebPresenter
import sobr_studio.beritabali.model.LinkBerita
import sobr_studio.beritabali.tab.ListBeritaAdapter

class TribunBaliFragment : Fragment(), TestURLView{

    private var links: MutableList<LinkBerita> = mutableListOf()
    lateinit var adapter: ListBeritaAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_list_berita,container,false)
        val rv = view.find<RecyclerView>(R.id.rv_main)
        val linearLayoutManager = LinearLayoutManager(context)

        rv.layoutManager = linearLayoutManager

        adapter = ListBeritaAdapter(links){
            startActivity<TestURL>()
        }

        rv.adapter = adapter

        NewsWebPresenter(this).getTribunBali()

        return view
    }

    override fun getWebsite(data:List<LinkBerita>) {
        links.clear()
        links.addAll(data)
        adapter.notifyDataSetChanged()

    }


}


