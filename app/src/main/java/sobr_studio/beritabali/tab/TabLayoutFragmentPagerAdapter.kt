package sobr_studio.beritabali.tab

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import sobr_studio.beritabali.tab.tribunbali.TribunBaliFragment

class TabLayoutFragmentPagerAdapter(fm:FragmentManager) : FragmentPagerAdapter(fm){
    override fun getItem(position: Int): Fragment? = when (position) {
        0-> TribunBaliFragment()
//        1-> TribunBaliFragment()
//        2-> TribunBaliFragment()
//        3-> TribunBaliFragment()
        else -> null
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "Tribun Bali"
//        1 -> "Tribun Bali"
//        2 -> "Tribun Bali"
//        3 -> "Tribun Bali"
        else -> ""
    }

    override fun getCount(): Int = 1
}